import pytest
from src.logic import REALLY_SAFE, sum_digits, is_coord_safe, check_area_safe, check_area_really_safe, check_area_really_safe, safe_area

expected_sum = 6

def test_sum_digits_should_return_0_if_not_a_number():
    expected = 0
    actual = sum_digits('RUBBISH')
    assert expected == actual

def test_sum_digits_should_return_sum_of_digits_in_positive_number(): 
    actual = sum_digits(123)
    assert expected_sum == actual

def test_sum_digits_should_return_sum_of_digits_in_negative_number(): 
    actual = sum_digits(-123)
    assert expected_sum == actual

def test_safe_area_should_return_false_if_sum_of_digits_exceeds_limit():
    assert False is is_coord_safe(888, 0)
    assert False is is_coord_safe(0, 888)

def test_safe_area_should_return_true_if_sum_of_digits_equals_limit():
    assert True is is_coord_safe(887, 0)
    assert True is is_coord_safe(0, 887)

def test_safe_area_should_return_true_if_sum_of_digits_below_limit():
    assert True is is_coord_safe(886, 0)
    assert True is is_coord_safe(0, 886)

def test_check_area_safe_should_return_coords_with_values_when_3_square():
    coords = check_area_safe(1, 1)
    assert 3 == len(coords)
    assert coords[0] == [False, True, False]
    assert coords[1] == [True, REALLY_SAFE, True]
    assert coords[2] == [False, True, False]

def test_check_area_really_safe_should_return_coords_with_values_when_3_square():
    coords = check_area_really_safe(check_area_safe(1, 1))
    assert 3 == len(coords)
    assert coords[0] == [False, True, False]
    assert coords[1] == [True, REALLY_SAFE, True]
    assert coords[2] == [False, True, False]

def test_check_area_safe_should_return_coords_with_values_when_5_square():
    coords = check_area_safe(1, 2)
    assert 5 == len(coords)
    assert coords[0] == [False, False, False, False, False]
    assert coords[1] == [False, False, True, False, False]
    assert coords[2] == [False, True, REALLY_SAFE, True, False]
    assert coords[3] == [False, False, True, False, False]
    assert coords[4] == [False, False, False, False, False]

def test_check_area_really_safe_should_return_coords_with_values_when_5_square():
    coords = check_area_really_safe(check_area_safe(1, 2))
    assert 5 == len(coords)
    assert coords[0] == [False, False, False, False, False]
    assert coords[1] == [False, False, REALLY_SAFE, False, False]
    assert coords[2] == [False, REALLY_SAFE, REALLY_SAFE, REALLY_SAFE, False]
    assert coords[3] == [False, False, REALLY_SAFE, False, False]
    assert coords[4] == [False, False, False, False, False]

def test_safe_area_should_return_expected_safe_area_for_small_limit():
    assert 1 == safe_area(1, 1)
    assert 5 == safe_area(1, 2)
    assert 5 == safe_area(1, 10)
    assert 13 == safe_area(2, 10)
    assert 25 == safe_area(3, 10)
    assert 361 == safe_area(23, 10)
