# Careful Robot

## Python Installation
Make sure you have Python installed. Python 3 or greater required.

## Requirements Installation

with Virtual Environment now active: -
Run the command below to install all the requirements to run the program

    pip3 install -r requirements.txt

## Testing the Program
In the folder **careful-robot** , run the command
 
    python3 -m pytest  

All the tests should pass

## Running the program

In your terminal, run the command below

    python3 src/index.py
