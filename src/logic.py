import numpy

DEFAULT_MAX_SUM = 23
DEFAULT_EXTENT = 750
REALLY_SAFE = 'really_safe'

### Return absolute sum of each digit in the number 'digits'
def sum_digits(digits):
    the_sum = 0
    try:
        string = str(abs(digits))
        the_sum = sum([int(i) for i in string])
    except Exception as e:
        print(e)
        the_sum = 0
    return the_sum    

### Optionally pass in exit_early and max_sum to sacrifice accuracy for speed
def is_coord_safe(x, y, max_sum = DEFAULT_MAX_SUM, exit_early = False):
    x_sum = sum_digits(x)
    if exit_early and x_sum > max_sum:
        return False
    y_sum = sum_digits(y)  
    return (x_sum + y_sum) <= max_sum

### Loop through each point and check if safe
def check_area_safe(max_sum = DEFAULT_MAX_SUM, extent = DEFAULT_EXTENT):
    coords = []
    x_s = numpy.arange(-extent, extent+1, 1)
    y_s = numpy.arange(-extent, extent+1, 1)
    for y in y_s:
        x_coords = []
        for x in x_s:
            x_coords.append(is_coord_safe(x, y, max_sum, True))
        coords.append(x_coords)
    ### Initialize the origin as `really safe` point
    coords[extent][extent] = REALLY_SAFE
    return coords

### Convert "really safe" points to 1s and then count them across x and y axes
def count_really_safe_points(coords):
    really_safe_points = [[1 if safety == REALLY_SAFE else 0 for safety in x_coords[1:-1]] for x_coords in coords[1:-1]]
    return sum([sum(i) for i in really_safe_points])

### Iteratively check if a safe point is beside a `really_safe` point
def check_area_really_safe(coords):
    changing = True
    while changing == True:
        changing = False
        # Skip the first and last rows and columns because these are just paddings for looking up/down/left/right
        for y, x_coords in enumerate(coords[1:-1], start=1):
            for x, is_safe in enumerate(x_coords[1:-1], start=1):
                if is_safe == REALLY_SAFE:
                    continue
                ### look up, down, left, right of the point being checked
                elif is_safe and (REALLY_SAFE in [coords[y-1][x], coords[y+1][x], coords[y][x-1], coords[y][x+1]]):
                    coords[y][x] = REALLY_SAFE
                    changing = True
    return coords

def safe_area(max_sum = DEFAULT_MAX_SUM, extent = DEFAULT_EXTENT):    
    coords = check_area_safe(max_sum, extent)

    coords = check_area_really_safe(coords)

    return count_really_safe_points(coords)
