from logic import safe_area
import time

start = time.time()

print('Calculating...')

print(f'Safe area: {safe_area()}')

print(f'Time taken: {time.time() - start}')
